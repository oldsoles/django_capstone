from django.test import TestCase
from restaurant.models import Menu
from restaurant.models import Booking

# Create your tests here.


class MenuTestCase(TestCase):
    def test_menu(self):
        menu = Menu(item="test", price=10, inventory=10, menu_item_description="test")
        self.assertEqual(menu.item, "test")
        self.assertEqual(menu.price, 10)
        self.assertEqual(menu.inventory, 10)
        self.assertEqual(menu.menu_item_description, "test")


class BookingTestCase(TestCase):
    def test_booking(self):
        booking = Booking(
            name="test", reservation_date="2021-04-01", persons=1, tableno=1
        )
        self.assertEqual(booking.name, "test")
        self.assertEqual(booking.reservation_date, "2021-04-01")
        self.assertEqual(booking.persons, 1)
        self.assertEqual(booking.tableno, 1)
