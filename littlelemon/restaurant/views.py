# from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse

from .models import Menu
from django.core import serializers
from .models import Booking
from datetime import datetime
import json
from .forms import BookingForm
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from rest_framework.views import APIView


def home(request):
    return render(request, "index.html")


def about(request):
    return render(request, "about.html")


def book(request):
    form = BookingForm()
    if request.method == "POST":
        form = BookingForm(request.POST)
        if form.is_valid():
            form.save()
    context = {"form": form}
    return render(request, "book.html", context)


class bookingsview(APIView):
    def get(self, request):
        bookings = Booking.objects.all()
        booking_json = serializers.serialize("json", bookings)
        return HttpResponse(booking_json, content_type="application/json")

    def post(self, request):
        data = json.load(request)
        exists = Booking.objects.filter(reservation_date=data["reservation_date"])
        if not exists:
            booking = Booking(
                name=data["name"],
                persons=data["persons"],
                reservation_date=data["reservation_date"],
                tableno=data["table_no"],
            )
            booking.save()
        else:
            return HttpResponse("{'error':1}", content_type="application/json")
        date = request.GET.get("date", datetime.today().date().strftime("%Y-%m-%d"))
        bookings = Booking.objects.all().filter(reservation_date=date)
        booking_json = serializers.serialize("json", bookings)
        return HttpResponse(booking_json, content_type="application/json")


class menuview(APIView):
    def get(self, request):
        menu_data = Menu.objects.all()
        menu_json = serializers.serialize("json", menu_data)
        return HttpResponse(menu_json, content_type="application/json")

    def post(self, request):
        data = json.load(request)
        menu = Menu(
            item=data["item"],
            price=data["price"],
            inventory=data["inventory"],
            menu_item_description=data["menu_item_description"],
        )
        menu.save()


@api_view()
@permission_classes([IsAuthenticated])
def securedview(request):
    return HttpResponse("Secured View")


def display_menu_item(request, pk=None):
    if pk:
        menu_item = Menu.objects.get(pk=pk)
    else:
        menu_item = ""
    return render(request, "menu_item.html", {"menu_item": menu_item})
