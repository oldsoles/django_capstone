from django.db import models


# Create your models here.
class Booking(models.Model):
    name = models.CharField(max_length=200)
    reservation_date = models.DateField()
    persons = models.PositiveSmallIntegerField()
    tableno = models.PositiveSmallIntegerField()

    def __str__(self): 
        return self.name


class Menu(models.Model):
   item = models.CharField(max_length=200) 
   price = models.PositiveSmallIntegerField(null=False) 
   inventory = models.PositiveSmallIntegerField()
   menu_item_description = models.TextField(max_length=1000, default='') 

   def __str__(self):
      return self.item
